---
title: "شهرام شایگانی"
summaryImage: "/img/team/shaygani.svg"
weight: 1
---
![شهرام شایگانی](/img/team/shaygani.svg)

## تحصیلات
* کارشناسی مهندسی عمران دانشگاه شیراز
* کارشناسی ارشد مهندسی سازه دانشگاه صنعتی خواجه نصیرالدین طوسی

## زمینه های کاری مورد علاقه
* Python
* GNU Octave
* Maxima
* Numerical Analysis

