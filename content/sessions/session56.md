---
title: "جلسه ۵۶"
description: "بحث آزاد"
date: "1395-02-04"
author: "Mahshid, mehdiMj"
draft: false
tags: ['lug talk', 'Contribution']
categories:
    - "sessions"
readmore: true
---
    جلسه ۵۶ شیراز لاگ بحث آزاد بود و طی آن تصمیم گرفته شد که یک ریپوزیتوری برای
شیراز لاگ تشکیل داده شود و توسط مهندس خزاعی و مهندس جفره و دیگر افراد حاضر در
جلسه کمی در مورد مسائل فنی مربوط به آن بحث و گفت و گو شد. 

